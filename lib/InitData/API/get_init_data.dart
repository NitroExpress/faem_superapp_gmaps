
import 'package:faem_super_app_gm/InitData/Model/InitData.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences sharedPreferences;
//String jwtToken = '';
var clientUuid, clientPhoneNumber, orderData;

Future<InitData> getInitData() async {
  InitData initData;
  clientUuid = null;
  sharedPreferences = await SharedPreferences.getInstance();
//  jwtToken = sharedPreferences.getString('jwt');
  var url = 'https://78.110.156.74:3005/api/v3/clients/initdata';
  var response = await http.get(url, headers: <String, String>{
    'Content-Type': 'application/json; charset=UTF-8',
    'Authorization': 'Bearer ${sharedPreferences.getString('jwt')}'
  });
  if(response.statusCode == 200) {
    var jsonResponse = json.decode(response.body);
    initData = new InitData.fromJson(jsonResponse);
    clientUuid = initData.clientUuid;
    clientPhoneNumber = initData.clientPhone;
    if (initData.ordersData != null) {
      orderData = initData.ordersData[0].uuid;
    } else {
      orderData = null;
    }
    print('InitData: ${response.body}');
  } else {
    var jsonResponse = json.decode(response.body);
    initData = new InitData.fromJson(jsonResponse);
    print('InitData ERROR: ${response.body}');
  }
  return initData;
}