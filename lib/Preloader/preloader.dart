import 'package:faem_super_app_gm/Config/config.dart';
import 'package:faem_super_app_gm/data/data.dart';
import 'package:faem_super_app_gm/main.dart';
import 'package:flutter/material.dart';


//  скрин на котором происходит получение данных из памяти и получение device_id
// играет роль прелоадера

class Preloader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: FutureBuilder<NecessaryDataForAuth>(
        future: NecessaryDataForAuth.getData(),
        builder: (BuildContext context,
            AsyncSnapshot<NecessaryDataForAuth> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // CartDataModel.getCart().then((value) {
            //   currentUser.cartModel = value;
            //   print('Cnjbn');
            // });
            necessaryDataForAuth = snapshot.data;
            if (necessaryDataForAuth.refreshToken == null ||
                necessaryDataForAuth.phoneNumber == null ||
                necessaryDataForAuth.name == null) {
              currentUser.isLoggedIn = false;
              // AmplitudeAnalytics.initialize(necessaryDataForAuth.device_id).then((value){
              //   AmplitudeAnalytics.analytics.logEvent('open_app');
              // });
              return MapSample();
            }
            print(necessaryDataForAuth.refreshToken);
            // AmplitudeAnalytics.initialize(necessaryDataForAuth.phoneNumber).then((value){
            //   AmplitudeAnalytics.analytics.logEvent('open_app');
            // });
            return MapSample();
          } else {
            return Center(
                child: Image(
              image: AssetImage('assets/images/faem.png'),
            ));
          }
        },
      ),
    );
  }
}
