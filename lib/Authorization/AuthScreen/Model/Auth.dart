class AuthData {
  int code;
  String message;
  int nextRequestTime;

  AuthData({
    this.code,
    this.message,
    this.nextRequestTime,
  });

  factory AuthData.fromJson(Map<String, dynamic> parsedJson) {
    return AuthData(
        code: parsedJson['code'],
        message: parsedJson['message'],
        nextRequestTime: parsedJson['next_request_time']);
  }
}
