import 'package:faem_super_app_gm/Authorization/AuthScreen/API/auth_data_pass.dart';
import 'package:faem_super_app_gm/Config/config.dart';
import 'package:faem_super_app_gm/Internet/check_internet.dart';
import 'package:faem_super_app_gm/data/data.dart';
import 'package:faem_super_app_gm/main.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../CodeScreen/View/code_screen.dart';

class AuthScreen extends StatefulWidget {
  final AuthSources source;
  AuthScreen({this.source = AuthSources.Drawer, Key key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState(source);
}

var controller = new MaskedTextController(mask: '+7 000 000-00-00');

class _AuthScreenState extends State<AuthScreen> {
  AuthSources source;
  String error = '';

  GlobalKey<ButtonState> buttonStateKey = new GlobalKey<ButtonState>();

  _AuthScreenState(this.source);

  @override
  void initState() {
    super.initState();
    controller.text = '';
    controller.afterChange = (String previous, String next) {
      if (next.length > previous.length) {
        controller.selection = TextSelection.fromPosition(
            TextPosition(offset: controller.text.length));
      }
      return false;
    };
    controller.beforeChange = (String previous, String next) {
      if (controller.text == '8') {
        controller.updateText('+7 ');
      }
      return true;
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                        padding: EdgeInsets.only(left: 0, top: 30),
                        child: Container(
                            height: 40,
                            width: 60,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 12, bottom: 12, right: 10),
                              child: SvgPicture.asset(
                                  'assets/svg_images/arrow_left.svg'),
                            ))),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 140),
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 91,
                  width: 300,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: Offset(0, 0), // changes position of shadow
                      ),
                    ],
                    color: Color(0xFF40475A),
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(10.0),
                    ),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          'Укажите ваш номер телефона',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: TextField(
                              autofocus: true,
                              controller: controller,
                              style: TextStyle(fontSize: 18),
                              textAlign: TextAlign.start,
                              maxLength: 16,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  borderSide: BorderSide.none,
                                ),
                                filled: true,
                                fillColor: Colors.white,
                                counterText: '',
                                contentPadding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.18,
                                ),
                                hintStyle: TextStyle(
                                  color: Color(0xFFC4C4C4),
                                ),
                                hintText: '+7 918 888-88-88',
                              ),
                              onChanged: (String value) {
                                if (value == '+7 8') {
                                  controller.text = '+7';
                                }
                                if (value.length == 16) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  buttonStateKey.currentState.setState(() {
                                    buttonStateKey.currentState.color =
                                        Color(0xFF40475A);
                                  });
                                }
                                currentUser.phone = value;
                                if (value.length >= 0 && value.length < 16
                                    // buttonStateKey.currentState.color !=
                                    //     Color(0xFF40475A)
                                    ) {
                                  buttonStateKey.currentState.setState(() {
                                    buttonStateKey.currentState.color =
                                        Color(0xF3C4C4C4);
                                  });
                                }
                                // else if (value.length == 0
                                //     // buttonStateKey.currentState.color !=
                                //     //     Color(0xF3C4C4C4)
                                // ) {
                                //   buttonStateKey.currentState.setState(() {
                                //     buttonStateKey.currentState.color =
                                //         Color(0xF3C4C4C4);
                                //   });
                                // }
                              },
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: 0,
              right: 0,
              child: GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(bottom: 85, top: 10),
                  child: Text.rich(
                    TextSpan(
                        text: 'Нажимая кнопку “Далее”, вы принимете условия\n',
                        style:
                            TextStyle(color: Color(0xff979797), fontSize: 13),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Пользовательского соглашения',
                              style: TextStyle(
                                  decoration: TextDecoration.underline),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  if (await Internet.checkConnection()) {
                                    if (await canLaunch(
                                        "https://faem.ru/legal/agreement")) {
                                      await launch(
                                          "https://faem.ru/legal/agreement");
                                    }
                                  } else {
                                    noConnection(context);
                                  }
                                }),
                          TextSpan(
                            text: ' и ',
                          ),
                          TextSpan(
                              text: 'Политики\nконфиденцальности',
                              style: TextStyle(
                                  decoration: TextDecoration.underline),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  if (await Internet.checkConnection()) {
                                    if (await canLaunch(
                                        "https://faem.ru/privacy")) {
                                      await launch("https://faem.ru/privacy");
                                    }
                                  } else {
                                    noConnection(context);
                                  }
                                }),
                        ]),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: 0,
              right: 0,
              child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Button(
                    key: buttonStateKey,
                    color: Color(0xFFC4C4C4),
                    source: source,
                  )),
            ),
          ],
        ));
  }
}

class Button extends StatefulWidget {
  final Color color;
  final AuthSources source;

  Button({Key key, this.color, this.source}) : super(key: key);

  @override
  ButtonState createState() {
    return new ButtonState(color, source);
  }
}

class ButtonState extends State<Button> {
  String error = '';
  Color color = Color(0xFFC4C4C4);
  AuthSources source;

  ButtonState(this.color, this.source);

  String validateMobile(String value) {
    String pattern = r'(^(?:[+]?7)[0-9]{10}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Укажите номер';
    } else if (!regExp.hasMatch(value)) {
      return 'Указан неверный номер';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FlatButton(
      child: Text('Далее',
          style: TextStyle(fontSize: 18.0, color: Color(0xFFFFFFFF))),
      color: color,
      splashColor: Color(0xFFF3F3F3),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        // side: BorderSide(
        //   width: 2,
        //   color: Color(0xF3C4C4C4),
        // ),
      ),
      padding: EdgeInsets.only(left: 130, top: 20, right: 130, bottom: 20),
      onPressed: () async {
        if (await Internet.checkConnection()) {
          currentUser.phone = currentUser.phone.replaceAll('-', '');
          currentUser.phone = currentUser.phone.replaceAll(' ', '');
          print(currentUser.phone);
          if (validateMobile(currentUser.phone) == null) {
            if (currentUser.phone[0] != '+') {
              currentUser.phone = '+' + currentUser.phone;
            }
            if (currentUser.phone != necessaryDataForAuth.phoneNumber) {
              await loadAuthData(
                  necessaryDataForAuth.deviceId, currentUser.phone, 'eda');
              necessaryDataForAuth.name = null;
              currentUser.isLoggedIn = false;
              Get.to(CodeScreen(source: source, number: controller.text,));
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => CodeScreen(
              //       source: source,
              //       number: controller.text,
              //     ),
              //   ),
              // );
            } else {
              print(necessaryDataForAuth.refreshToken);
              String refreshToken = await NecessaryDataForAuth.refreshTokenFunc(
                  necessaryDataForAuth.refreshToken,
                  necessaryDataForAuth.token,
                  necessaryDataForAuth.deviceId);
              if (refreshToken == null) {
                currentUser.isLoggedIn = false;
                await loadAuthData(
                    necessaryDataForAuth.deviceId, currentUser.phone, 'eda');
                Get.to(CodeScreen(source: source,));
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (context) => CodeScreen(source: source),
                //   ),
                // );
              } else {
                necessaryDataForAuth.refreshToken = refreshToken;
                currentUser.isLoggedIn = true;
                // await AmplitudeAnalytics.analytics.setUserId(necessaryDataForAuth.phoneNumber);
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => MapSample()),
                    (Route<dynamic> route) => false);
              }
            }
          } else {
            setState(() {
              error = 'Указан неверный номер';
            });
          }
        } else {
          noConnection(context);
        }
      },
    );
  }
}

enum AuthSources { Drawer, Cart }
