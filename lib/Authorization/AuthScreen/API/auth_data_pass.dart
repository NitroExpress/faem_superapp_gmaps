import 'dart:convert';

import 'package:faem_super_app_gm/Authorization/AuthScreen/Model/Auth.dart';
import 'package:http/http.dart';

Future<AuthData> loadAuthData(
  String deviceId,
  String phone,
  String service,
) async {
  AuthData authData;
  String jsonRequest = jsonEncode({
    "device_id": deviceId,
    "phone": phone,
    "service": service,
  });

  // print(deviceId + " " + phone + " " + service);

  String url = 'http://78.110.156.74:3005/api/v3/clients/new';
  Response response = await post(
    url,
    body: jsonRequest,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );
  print(response.body);
  if (response.statusCode == 200) {
    var jsonResponse = jsonDecode(response.body);
    authData = AuthData.fromJson(jsonResponse);
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
  return authData;
}
