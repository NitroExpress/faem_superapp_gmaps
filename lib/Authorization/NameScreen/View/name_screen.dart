import 'package:faem_super_app_gm/Authorization/AuthScreen/View/auth_screen.dart';
import 'package:faem_super_app_gm/Config/config.dart';
import 'package:faem_super_app_gm/Internet/check_internet.dart';
import 'package:faem_super_app_gm/data/data.dart';
import 'package:faem_super_app_gm/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NameScreen extends StatefulWidget {
  final AuthSources source;
  NameScreen({this.source = AuthSources.Drawer, Key key}) : super(key: key);

  @override
  NameScreenState createState() => NameScreenState(source);
}

class NameScreenState extends State<NameScreen> {
  GlobalKey<ButtonState> buttonStateKey = new GlobalKey<ButtonState>();
  TextEditingController nameFieldController = new TextEditingController();
  AuthSources source;

  NameScreenState(this.source);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Stack(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                      padding: EdgeInsets.only(left: 15, top: 40),
                      child: Container(
                          height: 40,
                          width: 60,
                          child: Padding(
                            padding:
                                EdgeInsets.only(top: 12, bottom: 12, right: 30),
                            child: SvgPicture.asset(
                                'assets/svg_images/arrow_left.svg'),
                          )))),
              onTap: () => Navigator.pop(context),
            ),
            Transform(
              transform: Matrix4.translationValues(0, 150, 0),
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 0, bottom: 15),
                        child: Text('Как вас зовут?',
                            style: TextStyle(
                              fontSize: 24,
                            )),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding:
                          EdgeInsets.only(right: 30, left: 30, bottom: 100),
                      child: Row(
                        children: <Widget>[
                          Flexible(
                            flex: 1,
                            child: Padding(
                                padding: EdgeInsets.only(left: 15, right: 15),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Color(0xF5F5F5F5),
                                      borderRadius: BorderRadius.circular(7.0),
                                      border: Border.all(
                                          width: 1.0,
                                          color: Color(0xF5F5F5F5))),
                                  child: TextField(
                                    controller: nameFieldController,
                                    textAlign: TextAlign.start,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    style: TextStyle(
                                      fontSize: 18,
                                    ),
                                    keyboardType: TextInputType.text,
                                    decoration: new InputDecoration(
                                      hintText: 'Ваше имя',
                                      contentPadding: EdgeInsets.only(left: 15),
                                      hintStyle: TextStyle(
                                          color: Color(0xFFB5B5B5),
                                          fontSize: 18),
                                      border: InputBorder.none,
                                      counterText: '',
                                    ),
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: 0,
              right: 0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Button(
                      key: buttonStateKey,
                      color: Color(0xFF40475A),
                      onTap: () async {
                        if (await Internet.checkConnection()) {
                          necessaryDataForAuth.name =
                              nameFieldController.text;
                          currentUser.isLoggedIn = true;
                          await NecessaryDataForAuth.saveData();
                          print(necessaryDataForAuth.name);

                          if (source == AuthSources.Cart) {
                            for (int i = 0; i < 3; i++)
                              Navigator.pop(context);
                            setState(() {});
                            return;
                          }
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => MapSample()),
                              (Route<dynamic> route) => false);
                        } else {
                          noConnection(context);
                        }
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}

class Button extends StatefulWidget {
  final Color color;
  final VoidCallback onTap;

  Button({Key key, this.color, this.onTap}) : super(key: key);

  @override
  ButtonState createState() {
    return new ButtonState(color, onTap: onTap);
  }
}

class ButtonState extends State<Button> {
  String error = '';
  Color color;
  final VoidCallback onTap;

  ButtonState(this.color, {this.onTap});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FlatButton(
      child:
          Text('Далее', style: TextStyle(fontSize: 18.0, color: Colors.white)),
      color: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.only(left: 130, top: 20, right: 130, bottom: 20),
      onPressed: () {
        onTap();
      },
    );
  }
}
