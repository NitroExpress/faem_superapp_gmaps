import 'dart:convert';
import 'dart:convert' as convert;

import 'package:faem_super_app_gm/Authorization/CodeScreen/Model/AuthCode.dart';
import 'package:faem_super_app_gm/InitData/API/get_init_data.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


Future<AuthCodeData> loadAuthCodeData(String deviceId, int code, String service) async {
  sharedPreferences = await SharedPreferences.getInstance();
  AuthCodeData authCodeData;
  var jsonRequest = jsonEncode({
    "device_id": deviceId,
    "code": code,
    "service": service,
  });
  var url = 'http://78.110.156.74:3005/api/v3/clients/verification';
  var response = await http.post(
    url,
    body: jsonRequest,
    headers: {'Content-Type': 'application/json; charset=UTF-8'},
  );
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    authCodeData = new AuthCodeData.fromJson(jsonResponse);
    await sharedPreferences.setString('jwt', authCodeData.token);
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
  print(response.body);
  return authCodeData;
}