import 'dart:async';

import 'package:faem_super_app_gm/Authorization/AuthScreen/View/auth_screen.dart';
import 'package:faem_super_app_gm/Authorization/CodeScreen/API/auth_code_data_pass.dart';
import 'package:faem_super_app_gm/Authorization/NameScreen/View/name_screen.dart';
import 'package:faem_super_app_gm/Config/config.dart';
import 'package:faem_super_app_gm/Internet/check_internet.dart';
import 'package:faem_super_app_gm/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

import '../../../data/data.dart';

class CodeScreen extends StatefulWidget {
  final String number;
  final AuthSources source;

  CodeScreen({this.source = AuthSources.Drawer, Key key, this.number})
      : super(key: key);

  @override
  _CodeScreenState createState() => _CodeScreenState(source);
}

class _CodeScreenState extends State<CodeScreen> {
  AuthSources source;

  String error = '';
  TextEditingController pinController = new TextEditingController();

  GlobalKey<ButtonState> buttonStateKey = new GlobalKey<ButtonState>();
  GlobalKey<TimerCountDownState> timerCountDownKey =
      new GlobalKey<TimerCountDownState>();

  _CodeScreenState(this.source);
  String code;

  void buttonColor() {
    if (code.length == 4 &&
        buttonStateKey.currentState.color != Color(0xFF40475A)) {
      buttonStateKey.currentState.setState(() {
        buttonStateKey.currentState.color = Color(0xFF40475A);
      });
    } else if (code.length < 4 &&
        buttonStateKey.currentState.color != Color(0xffC4C4C4)) {
      buttonStateKey.currentState.setState(() {
        buttonStateKey.currentState.color = Color(0xffC4C4C4);
      });
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                child: Padding(
                    padding: EdgeInsets.only(left: 0, top: 30),
                    child: Container(
                      width: 40,
                      height: 60,
                      child: Center(
                        child: SvgPicture.asset(
                            'assets/svg_images/arrow_left.svg'),
                      ),
                    )),
                onTap: () => Navigator.pop(context),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 120),
            child: Container(
              alignment: Alignment.topCenter,
              child: Text(
                'На номер ${widget.number} отправлен код',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 140),
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 110,
                width: 300,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                  color: Color(0xFF40475A),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Text(
                          'Введите код из смс',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 14.0),
                      child: Container(
                        height: 65.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(10),
                                bottomLeft: Radius.circular(10)),
                            color: Colors.white),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 6.0),
                            child: Container(
                              width: 200.0,
                              padding: EdgeInsets.only(bottom: 12),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(10),
                                      bottomLeft: Radius.circular(10)),
                                  color: Colors.white),
                              child: PinInputTextField(
                                keyboardType: TextInputType.number,
                                autoFocus: true,
                                controller: pinController,
                                pinLength: 4,
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly,
                                ],
                                decoration: UnderlineDecoration(
                                  textStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 22.0,
                                  ),
                                  colorBuilder: PinListenColorBuilder(
                                    Color(0xFF40475A),
                                    Color(0xFF40475A),
                                  ),
                                ),
                                // inputFormatter: <TextInputFormatter>[
                                //   WhitelistingTextInputFormatter.digitsOnly
                                // ],
                                onChanged: (String newPin) async {
                                  if (this.mounted) {
                                    setState(() {
                                      code = newPin;
                                      buttonColor();
                                    });
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: 10, bottom: 60, left: 0),
              child: Text(
                error,
                style: TextStyle(color: Colors.red, fontSize: 12),
              ),
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).viewInsets.bottom,
            left: 0,
            right: 0,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 90.0),
                child: Container(
                  child: new TimerCountDown(
                    codeScreenState: this,
                    key: timerCountDownKey,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              left: 0,
              right: 0,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Button(
                  key: buttonStateKey,
                  color: Color(0xffC4C4C4),
                  onTap: () async {
                    if (await Internet.checkConnection()) {
                      print(code);
                      authCodeData = await loadAuthCodeData(
                          necessaryDataForAuth.deviceId, int.parse(code),
                          'eda');
                      if (authCodeData != null) {
                        if (timerCountDownKey.currentState != null) {
                          timerCountDownKey.currentState.timerObject.cancel();
                          timerCountDownKey.currentState.dispose();
                        }
                        // await AmplitudeAnalytics.analytics.setUserId(currentUser.phone);
                        // AmplitudeAnalytics.analytics.logEvent('login');
                        necessaryDataForAuth.phoneNumber = currentUser.phone;
                        necessaryDataForAuth.refreshToken =
                            authCodeData.refreshToken.value;
                        necessaryDataForAuth.token = authCodeData.token;
                        await NecessaryDataForAuth.saveData();

                        if (necessaryDataForAuth.name == null) {
                          Navigator.push(
                            context,
                            new MaterialPageRoute(
                              builder: (context) => new NameScreen(
                                source: source,
                              ),
                            ),
                          );
                        } else {
                          currentUser.isLoggedIn = true;
                          if (source == AuthSources.Cart) {
                            for (int i = 0; i < 2; i++) Navigator.pop(context);

                            return;
                          }

                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => MapSample()),
                              (Route<dynamic> route) => false);
                        }
                      } else {
                        setState(() {
                          error = 'Вы ввели неверный смс код';
                        });
                      }
                    } else {
                      noConnection(context);
                    }
                  },
                ),
              ))
        ],
      ),
    );
  }
}

class TimerCountDown extends StatefulWidget {
  TimerCountDown({
    Key key,
    this.codeScreenState,
  }) : super(key: key);
  final _CodeScreenState codeScreenState;

  @override
  TimerCountDownState createState() {
    return new TimerCountDownState(codeScreenState: codeScreenState);
  }
}

class TimerCountDownState extends State<TimerCountDown> {
  TimerCountDownState({this.codeScreenState});

  final _CodeScreenState codeScreenState;
  Timer timerObject;
  int _start = 60;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    timerObject = new Timer.periodic(
      oneSec,
      (Timer timer) {
        return setState(
          () {
            try {
              if (_start < 1) {
                timer.cancel();
                timerObject.cancel();
              } else {
                _start = _start - 1;
              }
            } catch (e) {
              dispose();
            }
          },
        );
      },
    );
  }

  @override
  void dispose() {
    timerObject.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_start == 60) {
      startTimer();
    }
    return _start != 0
        ? Text('Получить новый код можно через $_start c',
            style: TextStyle(
              color: Color(0x97979797),
              fontSize: 13.0,
              letterSpacing: 1.2,
            ))
        : GestureDetector(
            child: Text(
              'Отправить код повторно',
              style: TextStyle(),
            ),
            onTap: () {
              codeScreenState.setState(() {});
            },
          );
  }
}

class Button extends StatefulWidget {
  final Color color;
  final AsyncCallback onTap;

  Button({Key key, this.color, this.onTap}) : super(key: key);

  @override
  ButtonState createState() {
    return new ButtonState(color, onTap);
  }
}

class ButtonState extends State<Button> {
  String error = '';
  TextField code1;
  TextField code2;
  TextField code3;
  TextField code4;
  Color color;
  final AsyncCallback onTap;

  ButtonState(this.color, this.onTap);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FlatButton(
      child:
          Text('Далее', style: TextStyle(fontSize: 18.0, color: Colors.white)),
      color: color,
      splashColor: Colors.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.only(left: 130, top: 20, right: 130, bottom: 20),
      onPressed: () async {
        if (await Internet.checkConnection()) {
          await onTap();
        } else {
          noConnection(context);
        }
      },
    );
  }

  String validateMobile(String value) {
    String pattern = r'(^(?:[+]?7)[0-9]{10}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Укажите норер';
    } else if (!regExp.hasMatch(value)) {
      return 'Указан неверный номер';
    }
    return null;
  }
}
