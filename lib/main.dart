import 'dart:async';
import 'package:get/get.dart';
import 'package:faem_super_app_gm/Authorization/AuthScreen/View/auth_screen.dart';
import 'package:faem_super_app_gm/Config/config.dart';
import 'package:faem_super_app_gm/Internet/check_internet.dart';
import 'package:faem_super_app_gm/Preloader/preloader.dart';
import 'package:faem_super_app_gm/Screens/InformationScreen/View/information_screen.dart';
import 'package:faem_super_app_gm/Screens/ProfileScreen/View/profile_screen.dart';
import 'package:faem_super_app_gm/data/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sliding_up_panel/flutter_sliding_up_panel.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'InitData/API/get_init_data.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Faem SuperApp',
      home: Preloader(),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

TextEditingController textEditingController = new TextEditingController();

// var globalCheck = false.obs;

class MapSampleState extends State<MapSample> {
  // final Location location = Location();
  LatLng myPos;
  Completer<GoogleMapController> _controller;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // StreamSubscription _getPositionSubscription;
  // final Set<Marker> _markers = {};
  // Position position;
  // static LatLng _initialPosition;
  // LatLng locationPoint;
  // var locationOptions;
  // var geolocator;
  // var lat, lng;

  @override
  void initState() {
    // TODO: implement initState
    scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.offset >=
          scrollController.position.maxScrollExtent &&
          !scrollController.position.outOfRange) {
        panelController.expand();
      } else if (scrollController.offset <=
          scrollController.position.minScrollExtent &&
          !scrollController.position.outOfRange) {
        panelController.anchor();
      } else {}
    });
    checkLoginStatus();
    super.initState();
    // _controller = Completer();
    // locationOptions = LocationOptions(distanceFilter: 10);
    // geolocator = Geolocator();
    // _getUserLocation();
    // _getLocation();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // _getPositionSubscription?.cancel();
  }

  checkLoginStatus() async {
    await getInitData();
    print(clientUuid);
    if (clientUuid != null) {
      // globalCheck.value = true;
      currentUser.isLoggedIn = true;
    } else {
      // globalCheck.value = false;
      currentUser.isLoggedIn = false;
    }
    print(currentUser.isLoggedIn);
  }

  // void _onMapCreated(GoogleMapController googleMapController) {
  //   lat = locationPoint.latitude;
  //   lng = locationPoint.longitude;
  //   // AddressLine(lat: lat, lng: lng,);
  //   setState(
  //     () {
  //       _controller.complete(googleMapController);
  //       _markers.add(
  //         Marker(
  //           draggable: true,
  //           markerId: MarkerId('Start Location'),
  //           position: locationPoint,
  //         ),
  //       );
  //     },
  //   );
  // }

  void dispatchAddress() {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: const Radius.circular(20),
          topRight: const Radius.circular(20),
        )),
        context: context,
        builder: (context) {
          return Container();
          // return Container(
          //     decoration: BoxDecoration(
          //         borderRadius: BorderRadius.only(
          //           topLeft: const Radius.circular(20),
          //           topRight: const Radius.circular(20),
          //         )
          //     ),
          //     height: MediaQuery.of(context).size.height * 0.9,
          //     width: MediaQuery.of(context).size.width,
          //     child: MaterialApp(
          //       debugShowCheckedModeBanner: false,
          //       theme: ThemeData(
          //         primaryColor: Color.fromRGBO(86, 86, 86, 1.00),
          //       ),
          //       home: HomeScreen(),
          //     ));
        });
  }

  // _getLocation() {
  //   _getPositionSubscription = Geolocator.getPositionStream().listen(
  //     (Position position) {
  //       print("PIS: ${position.toString()}");
  //       locationPoint = LatLng(position.latitude, position.longitude);
  //     },
  //   );
  // }
  //
  // void _getUserLocation() async {
  //   Position position = await Geolocator.getCurrentPosition();
  //   setState(() {
  //     _initialPosition = LatLng(position.latitude, position.longitude);
  //   });
  // }
  //
  // Future<void> _goToTheLake() async {
  //   final GoogleMapController googleMapController = await _controller.future;
  //   googleMapController.animateCamera(
  //     CameraUpdate.newCameraPosition(
  //       CameraPosition(
  //         target: locationPoint,
  //         zoom: 15,
  //       ),
  //     ),
  //   );
  // }

  // void _updatePosition(CameraPosition _position) {
  //   lat = _position.target.latitude;
  //   lng = _position.target.longitude;
  //   Marker marker = _markers.firstWhere(
  //           (p) => p.markerId == MarkerId('Start Location'),
  //       orElse: () => null);
  //   _markers.remove(marker);
  //   _markers.add(
  //     Marker(
  //       markerId: MarkerId('Start Location'),
  //       position: LatLng(_position.target.latitude, _position.target.longitude),
  //       draggable: true,
  //     ),
  //   );
  //   setState(() {});
  // }

  //initial camera position
  static final CameraPosition _preferredPos = CameraPosition(
    target: LatLng(43.03667, 44.66778),
    zoom: 11,
  );

  double maxHeight = 580;

  ScrollController scrollController;

  ///The controller of sliding up panel
  SlidingUpPanelController panelController = SlidingUpPanelController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      drawer: SafeArea(
        child: Drawer(
          child: Column(
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(top: 60),
              //   child: Center(
              //     child: Image(
              //       height: 97,
              //       width: 142,
              //       image: AssetImage('assets/images/faem.png'),
              //     ),
              //   ),
              // ),
              Expanded(
                child: ListView(
                  addAutomaticKeepAlives: false,
                  children: [SideBarItems()],
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 15, left: 15),
                  child: Text(
                    'Версия 1.0',
                    style: TextStyle(
                        fontSize: 17,
                        color: Color(0xFF424242),
                        letterSpacing: 0.45),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      body: Stack(
        children: [
          // SlidingUpPanel(
          //   // minHeight: 300,
          //   // maxHeight: maxHeight,
          //   panel: Container(
          //     height: MediaQuery.of(context).size.height * 0.5,
          //     width: MediaQuery.of(context).size.width,
          //     decoration: BoxDecoration(
          //         borderRadius: BorderRadius.only(
          //           topLeft: const Radius.circular(20),
          //           topRight: const Radius.circular(20),
          //         ),
          //         color: Colors.white),
          //     child: Padding(
          //       padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
          //       child: Column(
          //         children: [
          //           Container(
          //             decoration: BoxDecoration(
          //                 borderRadius: BorderRadius.circular(10),
          //                 color: Color(0xFFE6E6E6)),
          //             height: 53,
          //             child: Row(
          //               children: [
          //                 Image.asset('assets/images/super_car.png'),
          //                 Column(
          //                   children: [
          //                     Padding(
          //                       padding: const EdgeInsets.only(right: 35, top: 5),
          //                       child: Text('Откуда'),
          //                     ),
          //                     Padding(
          //                       padding:
          //                           const EdgeInsets.only(top: 8.0, left: 10),
          //                       child: Text('Куда поедем?'),
          //                     ),
          //                   ],
          //                 ),
          //               ],
          //             ),
          //           ),
          //           Align(
          //             alignment: Alignment.topLeft,
          //             child: Padding(
          //               padding: const EdgeInsets.only(left: 0, top: 10),
          //               child: GestureDetector(
          //                 child: Container(
          //                   height: 100,
          //                   width: 225,
          //                   decoration: BoxDecoration(
          //                       borderRadius: BorderRadius.circular(10),
          //                       color: Color(0xFFE6E6E6)),
          //                   child: Row(
          //                     children: [
          //                       Column(
          //                         children: [
          //                           Padding(
          //                             padding: const EdgeInsets.only(
          //                                 right: 30, top: 10),
          //                             child: Text('Еда'),
          //                           ),
          //                           Padding(
          //                             padding:
          //                                 const EdgeInsets.only(top: 5, left: 15),
          //                             child: Text('30-40 мин'),
          //                           ),
          //                         ],
          //                       ),
          //                       Padding(
          //                         padding: const EdgeInsets.only(left: 20),
          //                         child:
          //                             Image.asset('assets/images/super_food.png'),
          //                       ),
          //                     ],
          //                   ),
          //                 ),
          //                 onTap: () {
          //                   // dispatchAddress();
          //                 },
          //               ),
          //             ),
          //           )
          //         ],
          //       ),
          //     ),
          //   ),
          //   borderRadius: BorderRadius.only(
          //     topLeft: Radius.circular(10.0),
          //     topRight: Radius.circular(10.0),
          //   ),
          //   body: GoogleMap(
          //     onTap: (LatLng latLng) => setState(() => myPos = latLng),
          //     myLocationEnabled: true,
          //     myLocationButtonEnabled: false,
          //     zoomControlsEnabled: false,
          //     mapToolbarEnabled: false,
          //     compassEnabled: false,
          //     // markers: _markers,
          //     initialCameraPosition: _preferredPos,
          //     onMapCreated: (GoogleMapController controller) {
          //       _controller.complete(controller);
          //     },
          //   ),
          // ),
          GoogleMap(
            onTap: (LatLng latLng) => setState(() => myPos = latLng),
            myLocationEnabled: true,
            myLocationButtonEnabled: false,
            zoomControlsEnabled: false,
            mapToolbarEnabled: false,
            compassEnabled: false,
            // markers: _markers,
            initialCameraPosition: _preferredPos,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 50, left: 16, right: 15, bottom: 10),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8, top: 0),
                    child: InkWell(
                      child: SvgPicture.asset(
                        'assets/svg_images/home_menu.svg',
                        color: Colors.black,
                      ),
                      onTap: () {
                        _scaffoldKey.currentState.openDrawer();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
            SlidingUpPanelWidget(
              panelController: panelController,
              controlHeight: 80.0,
              anchor: 0.3,
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(10.0),
                      topRight: const Radius.circular(10.0),
                    ),
                    color: Colors.white,
                  ),
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.5,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(20),
                            topRight: const Radius.circular(20),
                          ),
                          color: Colors.white),
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 15, right: 15, top: 15),
                        child: Column(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color(0xFFE6E6E6)),
                              height: 53,
                              child: Row(
                                children: [
                                  Image.asset('assets/images/super_car.png'),
                                  Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            right: 35, top: 5),
                                        child: Text('Откуда'),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 8.0, left: 10),
                                        child: Text('Куда поедем?'),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 0, top: 10),
                                child: GestureDetector(
                                  child: Container(
                                    height: 100,
                                    width: 225,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Color(0xFFE6E6E6)),
                                    child: Row(
                                      children: [
                                        Column(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 30, top: 10),
                                              child: Text('Еда'),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 5, left: 15),
                                              child: Text('30-40 мин'),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20),
                                          child: Image.asset(
                                              'assets/images/super_food.png'),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    // dispatchAddress();
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  
          ),
            ),
          // Align(
          //   alignment: Alignment.bottomCenter,
          //   child: Container(
          //     width: double.infinity,
          //     height: 100,
          //     color: Colors.white,
          //     child: Column(
          //       children: [
          //         Align(child: Text(myPos.toString())),
          //         Align(child: Text('Yo')),
          //       ],
          //     ),
          //   ),
          // ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () => _getLocation(),
      //   child: Icon(Icons.navigation),
      // ),
    );
  }

// Future<void> _getLocation() async {
//   //check if app has permission to location service
//   PermissionStatus _locationPermissionStatus = await location.hasPermission();
//   // if not - request permission
//   if (_locationPermissionStatus != PermissionStatus.granted) {
//     _locationPermissionStatus = await location.requestPermission();
//   }
//   //get current user location
//   final LocationData _locationResult = await location.getLocation();
//   //move camera to users current location
//   final GoogleMapController controller = await _controller.future;
//   controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
//     target: LatLng(_locationResult.latitude, _locationResult.longitude),
//     zoom: 18,
//   )));
// }
}

class SideBarItems extends StatefulWidget {
  SideBarItems({Key key}) : super(key: key);

  @override
  SideBarItemsState createState() {
    return new SideBarItemsState();
  }
}

class SideBarItemsState extends State<SideBarItems> {
  List<Widget> getSideBarItems(bool isLogged) {
    List<Widget> allSideBarItems = [
      Padding(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: ListTile(
          leading: SvgPicture.asset('assets/svg_images/info.svg'),
          title: Text(
            'Информация',
            style: TextStyle(
                fontSize: 17, color: Color(0xFF424242), letterSpacing: 0.45),
          ),
          onTap: () async {
            if (await Internet.checkConnection()) {
              Get.to(InformationScreen());
              // Navigator.push(
              //   context,
              //   new MaterialPageRoute(
              //     builder: (context) => new InformationScreen(),
              //   ),
              // );
            } else {
              noConnection(context);
            }
          },
        ),
      ),
    ];

    if (isLogged) {
      allSideBarItems.insertAll(0, [
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: InkWell(
            child: Container(
              color: Color(0xFF09B44D),
              child: ListTile(
                title: Text(
                  necessaryDataForAuth.name ?? ' ',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17),
                ),
                subtitle: Text(
                  necessaryDataForAuth.phoneNumber ?? ' ',
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                trailing: GestureDetector(
                  child: SvgPicture.asset('assets/svg_images/pencil.svg'),
                ),
              ),
            ),
            onTap: () async {
              if (await Internet.checkConnection()) {
                Get.to(ProfileScreen());
                // Navigator.push(
                //   context,
                //   new MaterialPageRoute(
                //     builder: (context) => new ProfileScreen(),
                //   ),
                // );
              } else {
                noConnection(context);
              }
            },
          ),
        ),
        // Padding(
        //   padding: const EdgeInsets.only(top: 10, bottom: 10),
        //   child: ListTile(
        //     leading: SvgPicture.asset('assets/svg_images/pay.svg'),
        //     title: Text(
        //       'Способы оплаты',
        //       style: TextStyle(
        //           fontSize: 17, color: Color(0xFF424242), letterSpacing: 0.45),
        //     ),
        //     onTap: () async {
        //       if (await Internet.checkConnection()) {
        //         Navigator.push(
        //           context,
        //           new MaterialPageRoute(
        //             builder: (context) => new PaymentScreen(),
        //           ),
        //         );
        //       } else {
        //         noConnection(context);
        //       }
        //     },
        //   ),
        // ),
        Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: ListTile(
            leading: Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: SvgPicture.asset('assets/svg_images/order_story.svg'),
            ),
            title: Text(
              'История заказов',
              style: TextStyle(
                  fontSize: 17, color: Color(0xFF424242), letterSpacing: 0.45),
            ),
            onTap: () async {
              // if (await Internet.checkConnection()) {
              //   Navigator.push(
              //     context,
              //     new MaterialPageRoute(
              //       builder: (context) => new OrdersStoryScreen(),
              //     ),
              //   );
              // } else {
              //   noConnection(context);
              // }
            },
          ),
        ),
      ]);
      allSideBarItems.add(
        Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: ListTile(
            leading: SvgPicture.asset('assets/svg_images/exit.svg'),
            title: Text(
              'Выход',
              style: TextStyle(
                  fontSize: 17, color: Color(0xFF424242), letterSpacing: 0.45),
            ),
            onTap: () async {
              if (await Internet.checkConnection()) {
                necessaryDataForAuth.refreshToken = null;
                authCodeData.refreshToken.value = null;
                authCodeData.token = null;
                await NecessaryDataForAuth.saveData();
                // await LastAddressesModel.clear();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => Preloader()),
                    (Route<dynamic> route) => false);
              } else {
                noConnection(context);
              }
            },
          ),
        ),
      );
    } else {
      allSideBarItems.insert(
          0,
          Padding(
            padding: EdgeInsets.only(top: 0),
            child: ListTile(
                title: InkWell(
              child: Padding(
                padding: EdgeInsets.only(top: 0, bottom: 20),
                child: Text(
                  'Авторизоваться',
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFF424242),
                      letterSpacing: 0.45),
                ),
              ),
              onTap: () async {
                if (await Internet.checkConnection()) {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => new AuthScreen(),
                    ),
                  );
                } else {
                  noConnection(context);
                }
              },
            )),
          ));
    }
    return allSideBarItems;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: getSideBarItems(currentUser.isLoggedIn),
    );
  }
}

// class AddressLine extends StatefulWidget {
//   const AddressLine({Key key, this.lat, this.lng,})
//       : super(key: key);
//
//   final double lat, lng;
//
//   @override
//   _AddressLineState createState() => _AddressLineState();
// }
//
// class _AddressLineState extends State<AddressLine> {
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder<AddressData>(
//         future: findAddress(widget.lat, widget.lng),
//         builder: (context, AsyncSnapshot<AddressData>snapshot) {
//           if(snapshot.data == null) {
//             textEditingController.text = '';
//           } else {
//             textEditingController.text = snapshot.data.unrestrictedValue;
//           }
//           return Align(
//             alignment: Alignment.topCenter,
//             child: Padding(
//               padding: EdgeInsets.only(top: 150.0),
//               child: TextField(
//                 controller: textEditingController,
//                 enabled: false,
//                 textAlign: TextAlign.center,
//                 decoration: InputDecoration(
//                   border: InputBorder.none,
//                   focusedBorder: InputBorder.none,
//                   enabledBorder: InputBorder.none,
//                   errorBorder: InputBorder.none,
//                   disabledBorder: InputBorder.none,
//                 ),
//               ),
//             ),
//           );
//         }
//     );
//   }
// }
