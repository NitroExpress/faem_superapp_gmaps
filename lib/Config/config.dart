import 'dart:convert';
import 'dart:convert' as convert;

import 'package:device_id/device_id.dart';
import 'package:faem_super_app_gm/Authorization/CodeScreen/Model/AuthCode.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../data/data.dart';

// класс который занимается получением и хранением данных из памяти

class NecessaryDataForAuth {
  String deviceId;
  String phoneNumber;
  String refreshToken;
  String name;
  String token;
  String cityUuid;
  static NecessaryDataForAuth _necessaryDataForAuth;

  NecessaryDataForAuth(
      {this.deviceId,
      this.phoneNumber,
      this.token,
      this.cityUuid,
      this.refreshToken,
      this.name});

  static Future<NecessaryDataForAuth> getData() async {
    //    await Future.delayed(Duration(seconds: 4), () {});
    if (_necessaryDataForAuth != null) return _necessaryDataForAuth;
    String deviceId = await DeviceId.getID;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneNumber = prefs.getString('phone_number');
    String refreshToken = prefs.getString('refresh_token');
    String name = prefs.getString('name');
    // String city = prefs.getString();
    String cityUuid = prefs.getString('city_uuid');
    String token = prefs.getString('token');

    NecessaryDataForAuth result = new NecessaryDataForAuth(
      deviceId: deviceId,
      phoneNumber: phoneNumber,
      refreshToken: refreshToken,
      name: name,
      token: token,
      cityUuid: cityUuid,
    );

    print('1');
    refreshToken = await refreshTokenFunc(refreshToken, token, deviceId);

    result.refreshToken = refreshToken;
    _necessaryDataForAuth = result;
    if (refreshToken != null) {
      result.token = authCodeData.token;
      //await Centrifugo.connectToServer();
      await saveData();
    }

    return result;
  }

  static Future saveData() async {
    String phoneNumber = _necessaryDataForAuth.phoneNumber;
    String refreshToken = _necessaryDataForAuth.refreshToken;
    String name = _necessaryDataForAuth.name;
    String cityUuid = _necessaryDataForAuth.cityUuid;
    String token = _necessaryDataForAuth.token;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone_number', phoneNumber);
    prefs.setString('refresh_token', refreshToken);
    prefs.setString('name', name);
    prefs.setString('city_uuid', cityUuid);
    prefs.setString('token', token);
  }

  static Future<String> refreshTokenFunc(
      String refreshToken, String token, String deviceId) async {
    String result;
    var url = 'http://78.110.156.74:3005/api/v3/auth/clients/refresh';
    print(refreshToken);
    print('--------');
    print(token);
    if (refreshToken == null || token == null) {
      return null;
    }
    var response = await http.post(url,
        body: jsonEncode(
            {"device_id": deviceId, "service": "eda", "refresh": refreshToken}),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ' + token
        });
    print(response.body);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      authCodeData = AuthCodeData.fromJson(jsonResponse);
      result = authCodeData.refreshToken.value;
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
    return result;
  }

  static Future clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    _necessaryDataForAuth = null;
    necessaryDataForAuth = null;
  }
}
